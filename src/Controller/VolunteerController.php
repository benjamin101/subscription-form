<?php

namespace App\Controller;

use App\Entity\Volunteer;
use App\Entity\EmailRejection;

use App\Form\ConfirmationType;
use App\Form\VolunteerType;

use App\Entity\ConfirmationCode;

use Doctrine\ORM\EntityManagerInterface;

use Psr\Log\LoggerInterface;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Contracts\Translation\TranslatorInterface;

class VolunteerController extends AbstractController
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(TranslatorInterface $translator, LoggerInterface $logger)
    {
        $this->translator = $translator;
        $this->logger = $logger;
    }

    /**
     * Creation d'un nouveau volontaire.
     *
     * @Route("/", name="volunteer.create", methods={"GET", "POST"})
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     *
     * @return RedirectResponse|Response
     */
    public function create(Request $request, EntityManagerInterface $em) : Response
    {
        $volunteer = new Volunteer();
        $form = $this->createForm(VolunteerType::class, $volunteer);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $emailRejectionRepo = $em->getRepository(EmailRejection::class);

            $rejection = $emailRejectionRepo->findRecentlyRejectedEmail($volunteer->getEmail());

            if ($rejection !== null) {
                $expirationDateTime = $rejection->getExpiresAt();
                $timezone = $expirationDateTime->getTimezone()->getName();

                return $this->render('volunteer/reject.html.twig', [
                    'message' => $this->translator->trans('volunteer.rejection'),
                    'datetime' => $expirationDateTime->format($this->getParameter('datetime.format')),
                    'timezone' => $timezone,
                ]);
            }

            $volunteer->generateConfirmationCode();

            $this->container->get('session')->set('vol', $volunteer);

            return $this->redirectToRoute(
                'volunteer.confirm'
            );

        }

        return $this->render('volunteer/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Confirmation de creation d'un volontaire.
     *
     * @Route("/confirm", name="volunteer.confirm", methods={"GET", "POST"})
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     *
     * @return Response
     */
    public function confirm(Request $request, EntityManagerInterface $em) : Response
    {
        $volunteer = $this->container->get('session')->get('vol');

        if ($volunteer === null || !$volunteer instanceof Volunteer) {
            return $this->createNotFoundException();
        }

        $code = new ConfirmationCode();
        $form = $this->createForm(ConfirmationType::class, $code);

        var_dump($volunteer->getConfirmationCode());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // confirmation valide?
            if ($volunteer->getConfirmationCode() == $code->getValue()) {
                $volunteer->setIp($request->getClientIp());

                $em->persist($volunteer);
                $em->flush();

                $this->container->get('session')->remove('vol');

                return $this->render('volunteer/thanks.html.twig', [
                    'message' => $this->translator->trans('volunteer.confirmation'),
                ]);
            }

            $volunteer->confirmationAttempt();

            // nombre maximum de tentatives atteint?
            if ($volunteer->getRemainingConfirmationAttemptNumber() === 0) {
                $this->container->get('session')->remove('vol');

                $emailRejection = new EmailRejection($this->getParameter('volunteer.retry.after'));
                $emailRejection->setEmail($volunteer->getEmail());
                $emailRejection->setIp($request->getClientIp());

                $em->persist($emailRejection);
                $em->flush();

                return $this->render('volunteer/reject.html.twig');
            }

            // RAZ du formulaire
            $code = new ConfirmationCode();
            $form = $this->createForm(ConfirmationType::class, $code);
        }

        return $this->render('volunteer/confirm.html.twig', [
            'form' => $form->createView(),
            'remainingAttemptMessage' => $this->translator->transChoice(
                'confirmation.attempt.choices',
                $volunteer->getRemainingConfirmationAttemptNumber()
            ),
        ]);
    }
}
