<?php

namespace App\Controller\Admin;

use App\Entity\Contact;
use App\Form\ContactType;

use Doctrine\ORM\EntityManagerInterface;

use Knp\Component\Pager\PaginatorInterface;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\NotBlank;

class ContactController extends AbstractController
{
    /**
     * Creation d'un nouveau contact.
     *
     * @Route("/admin/contact/create", name="admin.contact.create", methods={"GET", "POST"})
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     *
     * @return RedirectResponse|Response
     */
    public function create(Request $request, EntityManagerInterface $em) : Response
    {
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($contact);
            $em->flush();

            return $this->redirectToRoute(
                'admin.contact.list'
            );
        }

        return $this->render('admin/contact/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Liste les contacts.
     *
     * @Route("/admin/contacts/{page}",
     *     name="admin.contact.list",
     *     methods="GET|POST",
     *     defaults={"page": 1},
     *     requirements={"page" = "\d+"}
     * )
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param PaginatorInterface $paginator
     * @param int $page
     *
     * @return Response
     */
    public function list(Request $request, EntityManagerInterface $em, PaginatorInterface $paginator, int $page) : Response
    {
        $repo = $em->getRepository(Contact::class);

        $contacts = $paginator->paginate(
            $repo->getPaginatedContacts(),
            $page,
            $this->getParameter('max_per_page'),
            [
                PaginatorInterface::DEFAULT_SORT_FIELD_NAME => 'c.createdAt',
                PaginatorInterface::DEFAULT_SORT_DIRECTION => 'DESC',
            ]
        );

        return $this->render('admin/contact/list.html.twig', [
            'contacts' => $contacts
        ]);
    }

    /**
     * Afficher un contact.
     *
     * @Route("/admin/contact/{id}/show", name="admin.contact.show", methods="GET|POST", requirements={"id" = "\d+"})
     *
     * @param Contact $contact
     *
     * @return Response
     */
    public function show(Contact $contact) : Response
    {
        return $this->render('admin/contact/show.html.twig', [
            'contact' => $contact,
        ]);
    }

    /**
     * Activer un contact.
     *
     * @Route("/admin/contact/{id}/activate", name="admin.contact.activate", methods="GET|POST", requirements={"id" = "\d+"})
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param Contact $contact
     *
     * @return Response
     */
    public function activate(Request $request, EntityManagerInterface $em, Contact $contact) : Response
    {
        if ($this->isCsrfTokenValid('cactivate' . $contact->getId(), $request->request->get('_token'))) {
            $contact->setActivated(true);

            $em->persist($contact);
            $em->flush();
        }

        return $this->redirectToRoute('admin.contact.list');
    }

    /**
     * Desactiver un contact.
     *
     * @Route("/admin/contact/{id}/disactivate", name="admin.contact.disactivate", methods="GET|POST", requirements={"id" = "\d+"})
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param Contact $contact
     *
     * @return Response
     */
    public function disactivate(Request $request, EntityManagerInterface $em, Contact $contact) : Response
    {
        if ($this->isCsrfTokenValid('cdisactivate' . $contact->getId(), $request->request->get('_token'))) {
            $contact->setActivated(false);

            $em->persist($contact);
            $em->flush();
        }

        return $this->redirectToRoute('admin.contact.list');
    }

    /**
     * Supprimer un contact.
     *
     * @Route("/admin/contact/{id}/delete", name="admin.contact.delete", methods="GET|POST", requirements={"id" = "\d+"})
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param Contact $contact
     *
     * @return Response
     */
    public function delete(Request $request, EntityManagerInterface $em, Contact $contact) : Response
    {
        if ($this->isCsrfTokenValid('cdelete' . $contact->getId(), $request->request->get('_token'))) {
            $em->remove($contact);
            $em->flush();
        }

        return $this->redirectToRoute('admin.contact.list');
    }
}
