<?php

namespace App\Controller\Admin;

use App\Entity\Volunteer;

use Doctrine\ORM\EntityManagerInterface;

use Knp\Component\Pager\PaginatorInterface;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\NotBlank;

class VolunteerController extends AbstractController
{
    /**
     * Liste les volontaires.
     *
     * @Route("/admin/volunteers/{page}",
     *     name="admin.volunteer.list",
     *     methods="GET|POST",
     *     defaults={"page": 1},
     *     requirements={"page" = "\d+"}
     * )
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param PaginatorInterface $paginator
     * @param int $page
     *
     * @return Response
     */
    public function list(Request $request, EntityManagerInterface $em, PaginatorInterface $paginator, int $page) : Response
    {
        $email = "";
        $form = $this->createForm(EmailType::class, $email, [ 'attr'=> ['placeholder' => 'user@example.com']]);
        $form->handleRequest($request);

        $repo = $em->getRepository(Volunteer::class);

        if ($form->isSubmitted() && $form->isValid()) {
            $volunteerId = $repo->findVolunteerIdByEmail($request->request->get('email'));

            if ($volunteerId !== null) {
                return $this->redirectToRoute('admin.volunteer.show', ['id' => $volunteerId]);
            }

            $this->addFlash('info', 'email.not.found');

            return $this->redirect($request->getUri());
        }

        $volunteers = $paginator->paginate(
            $repo->getPaginatedVolunteers(),
            $page,
            $this->getParameter('max_per_page'),
            [
                PaginatorInterface::DEFAULT_SORT_FIELD_NAME => 'v.createdAt',
                PaginatorInterface::DEFAULT_SORT_DIRECTION => 'DESC',
            ]
        );

        return $this->render('admin/volunteer/list.html.twig', [
            'volunteers' => $volunteers,
            'search_form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/volunteer/search", name="admin.volunteer.search_by_email", methods="GET|POST")
     *
     * @param EntityManagerInterface $em
     * @param PaginatorInterface $paginator
     * @param Request $request
     *
     * @return Response
     */
    public function searchByEmail(EntityManagerInterface $em, PaginatorInterface $paginator, Request $request)
    {
        if ($this->isCsrfTokenValid('emailsearch', $request->request->get('_token'))) {
            $volunteer = $paginator->paginate(
                $em->getRepository(Volunteer::class)->findVolunteerByEmail($request->request->get('email'))
            );

            return $this->render('admin/volunteer/list.html.twig', [
                'volunteers' => $volunteer,
            ]);
        }

        return $this->redirectToRoute('admin.volunteer.list');
    }

    /**
     * Envoyer ses identifiants a un volontaire.
     *
     * @Route("/admin/volunteer/{id}/sendcreds", name="admin.volunteer.sendcreds", methods="GET|POST", requirements={"id" = "\d+"})
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param Volunteer $volunteer
     *
     * @return Response
     */
    public function sendCredentials(Request $request, EntityManagerInterface $em, Volunteer $volunteer) : Response
    {
        if ($this->isCsrfTokenValid('sendcreds' . $volunteer->getId(), $request->request->get('_token'))) {
            $volunteer->setActivated(true);

            $em->persist($volunteer);
            $em->flush();
        }

        return $this->redirectToRoute('admin.volunteer.list');
    }

    /**
     * Afficher un volontaire.
     *
     * @Route("/admin/volunteer/{id}/show", name="admin.volunteer.show", methods="GET|POST", requirements={"id" = "\d+"})
     *
     * @param Volunteer $volunteer
     *
     * @return Response
     */
    public function show(Volunteer $volunteer) : Response
    {
        return $this->render('admin/volunteer/show.html.twig', [
            'vol' => $volunteer,
        ]);
    }

    /**
     * Supprimer un volontaire.
     *
     * @Route("/admin/volunteer/{id}/delete", name="admin.volunteer.delete", methods="GET|POST", requirements={"id" = "\d+"})
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param Volunteer $volunteer
     *
     * @return Response
     */
    public function delete(Request $request, EntityManagerInterface $em, Volunteer $volunteer) : Response
    {
        if ($this->isCsrfTokenValid('vdelete' . $volunteer->getId(), $request->request->get('_token'))) {
            $em->remove($volunteer);
            $em->flush();
        }

        return $this->redirectToRoute('admin.volunteer.list');
    }
}
