<?php

namespace App\Repository;

use App\Entity\Volunteer;

// use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class VolunteerRepository extends EntityRepository
{
    /**
     * @return Volunteer[]
     */
    public function findActiveVolunteers()
    {
        $qb = $this->createQueryBuilder('v')
            ->where('v.activated = :ac')
            ->setParameter('ac', true)
            ->orderBy('v.createdAt', 'DESC');
        return $qb->getQuery()->getResult();
    }

    /**
     * @param int $id
     *
     * @throws NonUniqueResultException
     *
     * @return Volunteer|null
     */
    public function findActiveVolunteer(int $id) : ?Volunteer
    {
        return $this->createQueryBuilder('v')
            ->where('v.id = :id')
            ->andWhere('v.activated = :ac')
            ->setParameter('id', $id)
            ->setParameter('ac', true)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param string $email
     *
     * @throws NonUniqueResultException
     *
     * @return int|null
     */
    public function findVolunteerIdByEmail(string $email) : ?int
    {
        $result = $this->createQueryBuilder('v')
          ->select('v.id')
          ->where('v.email = ?1')
          ->setParameter(1, $email)
          ->getQuery()
          ->getOneOrNullResult();

        return $result !== null ? $result['id'] : null;
    }

    /**
     * @return QueryBuilder
     */
    public function getPaginatedVolunteers() : QueryBuilder
    {
        return $this->getVolunteerQueryBuilder();
    }

    public function getFirstVolunteer() : array
    {
      return $this->getVolunteerQueryBuilder()->getQuery()->getResult();
    }

    /**
     * @return QueryBuilder
     */
    public function getPaginatedActiveVolunteers() : QueryBuilder
    {
        return $this->getVolunteerQueryBuilder()
            ->where('v.activated = :ac')
            ->setParameter('ac', true);
    }

    /**
     * @return QueryBuilder
     */
    protected function getVolunteerQueryBuilder()
    {
        return $this->createQueryBuilder('v')
            ->select('v.id')
            ->addSelect('v.email')
            ->addSelect('v.phone')
            ->addSelect('v.name')
            ->addSelect('v.comment')
            ->addSelect('v.ip')
            ->addSelect('v.activated')
            ->addSelect('v.createdAt');
    }
}
