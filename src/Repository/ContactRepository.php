<?php

namespace App\Repository;

use App\Entity\Contact;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class ContactRepository extends EntityRepository
{

    /**
     * Renvoie les contacts actifs.
     *
     * @return Contact[]
     */
    public function getActiveContacts()
    {
        return $this->createQueryBuilder('c')
            ->where('c.activated = :activated')
            ->setParameter('activated', true)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return QueryBuilder
     */
    public function getPaginatedContacts() : QueryBuilder
    {
        return $this->createQueryBuilder('c');
    }
}
