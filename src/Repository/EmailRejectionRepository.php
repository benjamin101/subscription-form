<?php

namespace App\Repository;

use App\Entity\EmailRejection;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;

class EmailRejectionRepository extends EntityRepository
{
    /**
     * @param string $email
     *
     * @throws NonUniqueResultException
     *
     * @return EmailRejection|null
     */
    public function findRecentlyRejectedEmail(string $email) : ?EmailRejection
    {
        return $this->createQueryBuilder('r')
        ->where('r.email = :email')
        ->andWhere('r.expiresAt > :dt')
        ->setParameter('email', $email)
        ->setParameter('dt', new \DateTime)
        ->getQuery()
        ->getOneOrNullResult();
    }
}
