<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rejet d'un email. Typiquement apres 3 erreurs de confirmation.
 *
 * @ORM\Entity(repositoryClass="App\Repository\EmailRejectionRepository")
 * @ORM\Table(name="email_rejections")
 * @ORM\HasLifecycleCallbacks()
 */
class EmailRejection
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=40)
     */
    private $ip;

    /**
     * @var \Datetime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \Datetime
     *
     * @ORM\Column(type="datetime")
     */
    private $expiresAt;

    /**
     * @param int $expiresAfter En heures.
     */
    public function __construct(int $expiresAfter)
    {
        $this->expiresAfter = $expiresAfter;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getEmail() : ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return self
     */
    public function setEmail(string $email) : self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getIp() : ?string
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     *
     * @return self
     */
    public function setIp(string $ip) : self
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedAt() : ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime|null
     */
    public function getExpiresAt() : ?\DateTime
    {
        return $this->expiresAt;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        if (!$this->createdAt) {
            $this->createdAt = new \DateTime();
            $this->expiresAt = (clone $this->createdAt)->modify('+' . $this->expiresAfter . 'hours');
        }
    }
}
