<?php

namespace App\Entity;

class ConfirmationCode
{
  const MIN = 10000;
  const MAX = 999999;

  private $value;

  /**
   * @return string
   */
  public function getValue() : ?string
  {
      return $this->value;
  }

  /**
   * @param string $value
   *
   * @return self
   */
  public function setValue(string $value) : self
  {
      $this->value = $value;

      return $this;
  }

  /**
   * Genere une valeur aleatoire.
   * @return int
   */
  public function generateValue() : int
  {
      $this->value = random_int(self::MIN, self::MAX);

      return $this->value;
  }
}
