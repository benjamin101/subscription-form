<?php

namespace App\Entity;

use App\Entity\ConfirmationCode;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Mapping\ClassMetadata;

/**
 * Candidat traductions.
 *
 * @ORM\Entity(repositoryClass="App\Repository\VolunteerRepository")
 * @ORM\Table(name="volunteers", indexes={@ORM\Index(name="email_idx", columns={"email"}), @ORM\Index(name="activated_idx", columns={"activated"})})
 * @ORM\HasLifecycleCallbacks()
 */
class Volunteer
{
    const MAX_CONFIRMATION_ATTEMPT_NB = 3;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, unique=true)
     *
     * Prenom
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, unique=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=40)
     */
    private $ip;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $login;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $secret;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $activated = false;

    /**
     * @var int
     */
    private $confirmationCode;

    /**
     * @var \Datetime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \Datetime
     *
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @var \Datetime|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $activatedAt;

    /**
     * @var int
     */
    private $confirmationAttemptNumber = 0;

    /**
     * @see https://captcha.com/doc/php/howto/symfony-captcha-bundle-integration.html
     * @see https://ourcodeworld.com/articles/read/1060/how-to-implement-botdetect-captcha-in-your-symfony-4-forms
     */
    private $captchaCode;

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata
          ->addConstraint(new UniqueEntity([
            'fields' => 'email',
            'message' => 'email.exists'
        ]))
          ->addConstraint(new UniqueEntity([
            'fields' => 'name',
            'message' => 'name.or.nickname.exists'
        ]));

        $metadata->addPropertyConstraint('email', new Assert\Email());
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName() : ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name) : self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail() : ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return self
     */
    public function setEmail(string $email) : self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone() : ?string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     *
     * @return self
     */
    public function setPhone(string $phone) : self
    {
        $this->phone = str_replace(['-', ' '], '', $phone);

        return $this;
    }

    /**
     * @return string
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     *
     * @return self
     */
    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return string
     */
    public function getIp() : ?string
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     *
     * @return self
     */
    public function setIp(string $ip) : self
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * @return bool
     */
    public function getActivated() : bool
    {
        return $this->activated;
    }

    /**
     * @param bool $activated
     *
     * @return self
     */
    public function setActivated(bool $activated) : self
    {
        if ($activated !== $this->activated) {
            $this->activated = $activated;

            if ($activated === true) {
                $this->activatedAt = new \Datetime();
            }
        }
        return $this;
    }

    /**
     * @return int|null
     */
    public function getConfirmationCode() : ?int
    {
        return $this->confirmationCode;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedAt() : ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime|null
     */
    public function getUpdatedAt() : ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @return \DateTime|null
     */
    public function getActivatedAt() : ?\DateTime
    {
        return $this->activatedAt;
    }

    /**
     * @return int
     */
    public function getConfirmationAttemptNumber() : int
    {
      return $this->confirmationAttemptNumber;
    }

    /**
     * @return string
     */
    public function getCaptchaCode()
    {
      return $this->captchaCode;
    }

    /**
     * @param string $_captchaCode
     */
    public function setCaptchaCode($_captchaCode)
    {
      $this->captchaCode = $_captchaCode;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        if (!$this->createdAt) {
            $this->createdAt = new \DateTime();
            $this->updatedAt = $this->createdAt;
        }
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * Confirmation attempt.
     *
     * @return bool true on success, false if maximum has been reached.
     */
    public function confirmationAttempt() : bool
    {
        if ($this->confirmationAttemptNumber < self::MAX_CONFIRMATION_ATTEMPT_NB) {
            ++$this->confirmationAttemptNumber;

            return true;
        }

        return false;
    }

    /**
     * @return int
     */
    public function getRemainingConfirmationAttemptNumber() : int
    {
      $result = self::MAX_CONFIRMATION_ATTEMPT_NB - $this->confirmationAttemptNumber;

      return $result > 0 ? $result : 0;
    }

    /**
     * Generation d'un code de confirmation.
     *
     * @param bool $_force true: regenere s'il est deja defini.
     */
    public function generateConfirmationCode(bool $_force=false)
    {
        if ($_force === true || $this->confirmationCode === null) {
            $confirmationCode = new ConfirmationCode();
            $this->confirmationCode = $confirmationCode->generateValue();
        }
    }
}
