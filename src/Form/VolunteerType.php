<?php

namespace App\Form;

use App\Entity\Volunteer;

use Captcha\Bundle\CaptchaBundle\Form\Type\CaptchaType;
use Captcha\Bundle\CaptchaBundle\Validator\Constraints\ValidCaptcha;

use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Contracts\Translation\TranslatorInterface;

use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class VolunteerType extends AbstractType
{
    private $translator;

    /**
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => $this->translator->trans('email.address'),
                'data' => 'jean@dupont.fr',
                'constraints' => [
                    new NotBlank(),
                    new Email()
                ]
            ])
            ->add('phone', TextType::class, [
                'label' => $this->translator->trans('phone'),
                'data' => '+3306000000',
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 8, 'max' => 50]),
                    new Regex(['pattern' => "/^\+?[0-9\- ]+$/"]),
                ]
            ])
            ->add('name', TextType::class, [
                'label' => $this->translator->trans('name.and.surname.or.nick'),
                'data' => 'Jean',
                'constraints' => [
//                    new NotBlank(),
                    new Length(['max' => 100]),
               ]
            ])
            ->add('comment', TextareaType::class, [
                'label' => $this->translator->trans('comment'),
                'data' => 'BLAH BLAH BLAH BLAH BLAH BLAH BLAH BLAH BLAH BLAH BLAH BLAH BLAH BLAH BLAH BLAH BLAH BLAH BLAH BLAH BLAH BLAH BLAH BLAH BLAH BLAH BLAH BLAH BLAH BLAH',
                'required' => false,
                'constraints' => [
//                    new NotBlank(),
                    new Length([/*'min' => 100, */'max' => 750]),
                ]
            ])
            ->add('captchaCode', CaptchaType::class, [
                'label' => $this->translator->trans('captcha'),
                'captchaConfig' => 'VolunteerCapatcha',
                'constraints' => [
                    new ValidCaptcha([
                        'message' => 'Invalid captcha, please try again',
                    ]),
                ],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Volunteer::class,
        ]);
    }
}
